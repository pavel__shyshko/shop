package org.example.work.mapper;

import javax.annotation.processing.Generated;
import org.example.work.dto.StoragePointDto;
import org.example.work.entity.StoragePoint;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-06-18T22:48:09+0300",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 17.0.6 (Oracle Corporation)"
)
@Component
public class StoragePointMapperImpl implements StoragePointMapper {

    @Override
    public StoragePointDto storagePointToStoragePointDto(StoragePoint storagePoint) {
        if ( storagePoint == null ) {
            return null;
        }

        StoragePointDto storagePointDto = new StoragePointDto();

        storagePointDto.setId( storagePoint.getId() );
        storagePointDto.setName( storagePoint.getName() );
        storagePointDto.setCity( storagePoint.getCity() );
        storagePointDto.setStreet( storagePoint.getStreet() );
        storagePointDto.setBuilding( storagePoint.getBuilding() );

        return storagePointDto;
    }

    @Override
    public StoragePoint storagePointDtoToStoragePoint(StoragePointDto storagePointDto) {
        if ( storagePointDto == null ) {
            return null;
        }

        StoragePoint storagePoint = new StoragePoint();

        storagePoint.setId( storagePointDto.getId() );
        storagePoint.setName( storagePointDto.getName() );
        storagePoint.setCity( storagePointDto.getCity() );
        storagePoint.setStreet( storagePointDto.getStreet() );
        storagePoint.setBuilding( storagePointDto.getBuilding() );

        return storagePoint;
    }
}
