package org.example.work.mapper;

import javax.annotation.processing.Generated;
import org.example.work.dto.UserDetailsDto;
import org.example.work.entity.UserDetails;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-06-18T22:48:09+0300",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 17.0.6 (Oracle Corporation)"
)
@Component
public class UserDetailsMapperImpl implements UserDetailsMapper {

    @Override
    public UserDetailsDto detailsToUserDetailsDto(UserDetails userDetails) {
        if ( userDetails == null ) {
            return null;
        }

        UserDetailsDto userDetailsDto = new UserDetailsDto();

        userDetailsDto.setUserId( userDetails.getUserId() );
        userDetailsDto.setFirstName( userDetails.getFirstName() );
        userDetailsDto.setLastName( userDetails.getLastName() );
        userDetailsDto.setBirthDate( userDetails.getBirthDate() );

        return userDetailsDto;
    }

    @Override
    public UserDetails userDetailsDtoToUserDetails(UserDetailsDto userDetailsDto) {
        if ( userDetailsDto == null ) {
            return null;
        }

        UserDetails userDetails = new UserDetails();

        userDetails.setUserId( userDetailsDto.getUserId() );
        userDetails.setFirstName( userDetailsDto.getFirstName() );
        userDetails.setLastName( userDetailsDto.getLastName() );
        userDetails.setBirthDate( userDetailsDto.getBirthDate() );

        return userDetails;
    }
}
