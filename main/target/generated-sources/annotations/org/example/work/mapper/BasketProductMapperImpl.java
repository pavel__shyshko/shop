package org.example.work.mapper;

import javax.annotation.processing.Generated;
import org.example.work.dto.BasketProductDto;
import org.example.work.entity.BasketProduct;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-06-18T22:48:09+0300",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 17.0.6 (Oracle Corporation)"
)
@Component
public class BasketProductMapperImpl implements BasketProductMapper {

    @Override
    public BasketProductDto basketProductToBasketProductDto(BasketProduct basketProduct) {
        if ( basketProduct == null ) {
            return null;
        }

        BasketProductDto basketProductDto = new BasketProductDto();

        basketProductDto.setUserId( basketProduct.getUserId() );
        basketProductDto.setProductId( basketProduct.getProductId() );
        basketProductDto.setProductCount( basketProduct.getProductCount() );

        return basketProductDto;
    }

    @Override
    public BasketProduct basketProductDtoToBasketProduct(BasketProductDto basketProductDto) {
        if ( basketProductDto == null ) {
            return null;
        }

        BasketProduct basketProduct = new BasketProduct();

        basketProduct.setUserId( basketProductDto.getUserId() );
        basketProduct.setProductId( basketProductDto.getProductId() );
        basketProduct.setProductCount( basketProductDto.getProductCount() );

        return basketProduct;
    }
}
