package org.example.work.mapper;

import javax.annotation.processing.Generated;
import org.example.work.dto.ProductDto;
import org.example.work.entity.Product;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-06-18T22:48:09+0300",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 17.0.6 (Oracle Corporation)"
)
@Component
public class ProductMapperImpl implements ProductMapper {

    @Override
    public ProductDto productToProductDto(Product product) {
        if ( product == null ) {
            return null;
        }

        ProductDto productDto = new ProductDto();

        productDto.setId( product.getId() );
        productDto.setName( product.getName() );
        productDto.setBrandId( product.getBrandId() );
        productDto.setPrice( product.getPrice() );
        productDto.setColor( product.getColor() );
        productDto.setSize( product.getSize() );
        productDto.setDiscount( product.getDiscount() );

        return productDto;
    }

    @Override
    public Product productDtoToProduct(ProductDto productDto) {
        if ( productDto == null ) {
            return null;
        }

        Product product = new Product();

        product.setId( productDto.getId() );
        product.setName( productDto.getName() );
        product.setBrandId( productDto.getBrandId() );
        product.setPrice( productDto.getPrice() );
        product.setColor( productDto.getColor() );
        product.setSize( productDto.getSize() );
        product.setDiscount( productDto.getDiscount() );

        return product;
    }
}
