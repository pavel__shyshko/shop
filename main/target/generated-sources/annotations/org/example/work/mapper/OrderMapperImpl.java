package org.example.work.mapper;

import javax.annotation.processing.Generated;
import org.example.work.dto.OrderDto;
import org.example.work.entity.Order;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2024-06-18T22:48:08+0300",
    comments = "version: 1.5.5.Final, compiler: javac, environment: Java 17.0.6 (Oracle Corporation)"
)
@Component
public class OrderMapperImpl implements OrderMapper {

    @Override
    public OrderDto orderToOrderDto(Order order) {
        if ( order == null ) {
            return null;
        }

        OrderDto orderDto = new OrderDto();

        orderDto.setId( order.getId() );
        orderDto.setUserId( order.getUserId() );
        orderDto.setStorageId( order.getStorageId() );
        orderDto.setRegistrationDate( order.getRegistrationDate() );
        orderDto.setDeliveryDate( order.getDeliveryDate() );
        orderDto.setPaymentMethodId( order.getPaymentMethodId() );
        orderDto.setTotalPrice( order.getTotalPrice() );

        return orderDto;
    }

    @Override
    public Order orderDtoToOrder(OrderDto orderDto) {
        if ( orderDto == null ) {
            return null;
        }

        Order order = new Order();

        order.setId( orderDto.getId() );
        order.setUserId( orderDto.getUserId() );
        order.setStorageId( orderDto.getStorageId() );
        order.setRegistrationDate( orderDto.getRegistrationDate() );
        order.setDeliveryDate( orderDto.getDeliveryDate() );
        order.setPaymentMethodId( orderDto.getPaymentMethodId() );
        order.setTotalPrice( orderDto.getTotalPrice() );

        return order;
    }
}
