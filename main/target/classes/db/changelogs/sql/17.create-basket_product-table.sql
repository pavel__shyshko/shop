CREATE TABLE basket_product (
  user_id integer REFERENCES users(user_id),
  product_id integer REFERENCES products(product_id),
  PRIMARY KEY (user_id, product_id),
  product_count integer
);