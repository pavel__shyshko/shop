CREATE TABLE orders (
  order_id integer GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
  user_id integer REFERENCES users(user_id),
  storage_id integer REFERENCES storage_points(storage_id),
  registration_date timestamp NOT NULL,
  delivery_date timestamp, 
  payment_method_id integer REFERENCES payment_methods(payment_method_id),
  all_products_price decimal NOT NULL,
  order_discount decimal,
  delivery_price decimal,
  total_price decimal  
);