CREATE TABLE users (
  user_id integer GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,  
  user_name varchar(30) NOT NULL,
  user_password varchar (30) NOT NULL,
  email varchar(30) NOT NULL,
  personal_discount decimal,
  is_active BOOLEAN
);