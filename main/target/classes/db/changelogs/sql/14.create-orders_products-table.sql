CREATE TABLE orders_products (
  order_id integer REFERENCES orders(order_id),
  product_id integer REFERENCES products(product_id),
  PRIMARY KEY (order_id, product_id),
  product_count integer
);