CREATE TABLE users_roles (
  user_id integer REFERENCES users(user_id),
  role_id integer REFERENCES roles(role_id),
  PRIMARY KEY (user_id, role_id)
);