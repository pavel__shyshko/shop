CREATE TABLE price_change (
  product_id integer REFERENCES products(product_id),
  price_change_date timestamp,
  price decimal NOT NULL
);