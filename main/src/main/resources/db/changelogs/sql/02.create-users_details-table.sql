CREATE TABLE users_details (
  user_id integer PRIMARY KEY REFERENCES users(user_id),
  firstname varchar(30) NOT NULL,
  surname varchar(30) NOT NULL,
  birth_date timestamp NOT NULL,
  registration_date timestamp NOT NULL,
  telephone varchar (20) NOT NULL
);