CREATE TABLE brands (
  brand_id integer GENERATED BY DEFAULT AS IDENTITY PRIMARY KEY,
  name varchar(50) NOT NULL
);