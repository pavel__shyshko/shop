CREATE TABLE storages_products (
  storage_id integer REFERENCES storage_points(storage_id),
  product_id integer REFERENCES products(product_id),
  PRIMARY KEY (storage_id, product_id),
  product_count integer,
  delivery_date timestamp
);