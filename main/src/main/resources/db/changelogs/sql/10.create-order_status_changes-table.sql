CREATE TABLE order_status_changes(
  order_id integer REFERENCES orders(order_id),
  order_status_id integer REFERENCES order_statuses(order_status_id),
  status_change_date timestamp,
  PRIMARY KEY (order_id, order_status_id)
);