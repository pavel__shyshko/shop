package org.example;

import org.example.work.controller.impl.*;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("org.example.work")
public class Application {
    public static void main(String[] args) {
        ApplicationContext context = new AnnotationConfigApplicationContext(Application.class);
        System.out.println("Product entity");
        ProductControllerImpl productController = context.getBean(ProductControllerImpl.class);
        productController.create("{\"id\": \"1\", \"name\": \"Ball\", \"brandId\": \"3\", \"price\": \"150\", \"color\": \"black-white\", \"size\": \"4\", \"discount\": \"5\"}");
        productController.create("{\"id\": \"2\", \"name\": \"T-shirt\", \"brandId\": \"7\", \"price\": \"55\", \"color\": \"red\", \"size\": \"XL\", \"discount\": \"10\"}");
        System.out.println("First product: " + productController.read(0));
        productController.delete(1);
        productController.update("{\"id\": \"1\", \"name\": \"Ball\", \"brandId\": \"3\", \"price\": \"170\", \"color\": \"black-white\", \"size\": \"4\", \"discount\": \"0\"}");
        System.out.println("First product updated " + productController.read(0));
        System.out.println("_____________________________________________________________________________________________________");

        System.out.println("User entity");
        UserControllerImpl userController = context.getBean(UserControllerImpl.class);
        userController.create("{\"id\": \"1\", \"login\": \"Iv_iv\", \"email\": \"iv2000@mail.ru\", \"personalDiscount\": \"7\"}");
        userController.create("{\"id\": \"2\", \"login\": \"Pet_pet\", \"email\": \"peter2001@mail.ru\", \"personalDiscount\": \"5\"}");
        System.out.println("First user: " + userController.read(0));
        userController.delete(1);
        userController.update("{\"id\": \"1\", \"login\": \"Ivan\", \"email\": \"iv2000@mail.ru\", \"personalDiscount\": \"7\"}");
        System.out.println("First user updated " + userController.read(0));
        System.out.println("_____________________________________________________________________________________________________");

        System.out.println("UserDetails entity");
        UserDetailsControllerImpl userDetailsController = context.getBean(UserDetailsControllerImpl.class);
        userDetailsController.create("{\"userId\": \"1\", \"firstName\": \"Ivan\", \"lastName\": \"Ivanov\", \"birthDate\": \"2000-02-24\"}");
        userDetailsController.create("{\"userId\": \"2\", \"firstName\": \"Peter\", \"lastName\": \"Petrov\", \"birthDate\": \"2001-12-27\"}");
        System.out.println("First userDetails: " + userDetailsController.read(0));
        userDetailsController.delete(1);
        userDetailsController.update("{\"userId\": \"1\", \"firstName\": \"Ivan\", \"lastName\": \"Ivanovskiy\", \"birthDate\": \"2000-02-24\"}");
        System.out.println("First userDetails updated " + userDetailsController.read(0));
        System.out.println("_____________________________________________________________________________________________________");

        System.out.println("StoragePoint entity");
        StoragePointControllerImpl storagePointController = context.getBean(StoragePointControllerImpl.class);
        storagePointController.create("{\"id\": \"1\", \"name\": \"Storage 1\", \"city\": \"Minsk\", \"street\": \"Nemiga\", \"building\": \"25\"}");
        storagePointController.create("{\"id\": \"2\", \"name\": \"Storage 2\", \"city\": \"Grodno\", \"street\": \"Dubko\", \"building\": \"1\"}");
        System.out.println("First storagePoint: " + storagePointController.read(0));
        storagePointController.delete(1);
        storagePointController.update("{\"id\": \"1\", \"name\": \"Storage 1\", \"city\": \"Minsk\", \"street\": \"Lenina\", \"building\": \"16\"}");
        System.out.println("First storagePoint updated " + storagePointController.read(0));
        System.out.println("_____________________________________________________________________________________________________");



        System.out.println("Order entity");
        OrderControllerImpl orderController = context.getBean(OrderControllerImpl.class);
        orderController.create("{\"id\": \"1\", \"userId\": \"17\", \"storageId\": \"1\", \"registrationDate\": \"2024-04-11\", \"deliveryDate\": \"2024-04-15\", \"paymentMethodId\": \"1\", \"totalPrice\": \"330.50\"}");
        orderController.create("{\"id\": \"2\", \"userId\": \"105\", \"storageId\": \"2\", \"registrationDate\": \"2024-04-20\", \"deliveryDate\": \"2024-04-25\", \"paymentMethodId\": \"2\", \"totalPrice\": \"170.15\"}");
        System.out.println("First order: " + orderController.read(0));
        orderController.delete(1);
        orderController.update("{\"id\": \"1\", \"userId\": \"17\", \"storageId\": \"1\", \"registrationDate\": \"2024-04-11\", \"deliveryDate\": \"2024-04-17\", \"paymentMethodId\": \"1\", \"totalPrice\": \"310.50\"}");
        System.out.println("First order updated " + orderController.read(0));
        System.out.println("_____________________________________________________________________________________________________");

        System.out.println("Category entity");
        CategoryControllerImpl categoryController = context.getBean(CategoryControllerImpl.class);
        categoryController.create("{\"id\": \"1\", \"name\": \"cloth\"}");
        categoryController.create("{\"id\": \"2\", \"name\": \"shoes\"}");
        System.out.println("First category: " + categoryController.read(0));
        categoryController.delete(1);
        categoryController.update("{\"id\": \"1\", \"name\": \"team sports\"}");
        System.out.println("First category updated " + categoryController.read(0));
        System.out.println("_____________________________________________________________________________________________________");

        System.out.println("Brand entity");
        BrandControllerImpl brandController = context.getBean(BrandControllerImpl.class);
        brandController.create("{\"id\": \"1\", \"name\": \"Adidas\"}");
        brandController.create("{\"id\": \"2\", \"name\": \"Puma\"}");
        System.out.println("First brand: " + brandController.read(0));
        brandController.delete(1);
        brandController.update("{\"id\": \"1\", \"name\": \"Nike\"}");
        System.out.println("First brand updated " + brandController.read(0));
        System.out.println("_____________________________________________________________________________________________________");
    }
}
