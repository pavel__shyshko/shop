package org.example.work.controller.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.work.controller.StoragePointController;
import org.example.work.dto.StoragePointDto;
import org.example.work.service.StoragePointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class StoragePointControllerImpl implements StoragePointController {
    private final StoragePointService storagePointService;
    private final ObjectMapper objectMapper;

    @Autowired
    public StoragePointControllerImpl(StoragePointService storagePointService, ObjectMapper objectMapper) {
        this.storagePointService = storagePointService;
        this.objectMapper = objectMapper;
    }

    @Override
    public void create(String json) {
        try {
            storagePointService.create(objectMapper.readValue(json, StoragePointDto.class));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String read(int id) {
        try {
            return objectMapper.writeValueAsString(storagePointService.read(id));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String update(String json) {
        try {
            return objectMapper.writeValueAsString(storagePointService.update(objectMapper.readValue(json, StoragePointDto.class)));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(int id) {
        storagePointService.delete(id);
    }
}
