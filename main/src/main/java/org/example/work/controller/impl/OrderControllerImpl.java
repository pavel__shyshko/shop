package org.example.work.controller.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.work.controller.OrderController;
import org.example.work.dto.OrderDto;
import org.example.work.service.OrderService;
import org.springframework.stereotype.Component;

@Component
public class OrderControllerImpl implements OrderController {
    private final OrderService orderService;
    private final ObjectMapper objectMapper;

    public OrderControllerImpl(OrderService orderService, ObjectMapper objectMapper) {
        this.orderService = orderService;
        this.objectMapper = objectMapper;
    }

    @Override
    public void create(String json) {
        try {
            orderService.create(objectMapper.readValue(json, OrderDto.class));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String read(int id) {
        try {
            return objectMapper.writeValueAsString(orderService.read(id));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String update(String json) {
        try {
            return objectMapper.writeValueAsString(orderService.update(objectMapper.readValue(json, OrderDto.class)));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(int id) {
        orderService.delete(id);
    }
}
