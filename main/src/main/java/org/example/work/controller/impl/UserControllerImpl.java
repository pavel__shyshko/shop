package org.example.work.controller.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.work.controller.UserController;
import org.example.work.dto.UserDto;
import org.example.work.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserControllerImpl implements UserController {

    private final UserService userService;
    private final ObjectMapper objectMapper;

    @Autowired
    public UserControllerImpl(UserService userService, ObjectMapper objectMapper) {
        this.userService = userService;
        this.objectMapper = objectMapper;
    }

    @Override
    public void create(String json) {
        try {
            userService.create(objectMapper.readValue(json, UserDto.class));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String read(int id) {
        try {
            return objectMapper.writeValueAsString(userService.read(id));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String update(String json) {
        try {
            return objectMapper.writeValueAsString(userService.update(objectMapper.readValue(json, UserDto.class)));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(int id) {
        userService.delete(id);
    }
}
