package org.example.work.controller.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.work.controller.CategoryController;
import org.example.work.dto.CategoryDto;
import org.example.work.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class CategoryControllerImpl implements CategoryController {
    private final CategoryService categoryService;
    private final ObjectMapper objectMapper;

    @Autowired
    public CategoryControllerImpl(CategoryService categoryService, ObjectMapper objectMapper) {
        this.categoryService = categoryService;
        this.objectMapper = objectMapper;
    }

    @Override
    public void create(String json) {
        try {
            categoryService.create(objectMapper.readValue(json, CategoryDto.class));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String read(int id) {
        try {
            return objectMapper.writeValueAsString(categoryService.read(id));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String update(String json) {
        try {
            return objectMapper.writeValueAsString(categoryService.update(objectMapper.readValue(json, CategoryDto.class)));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(int id) {
        categoryService.delete(id);
    }
}
