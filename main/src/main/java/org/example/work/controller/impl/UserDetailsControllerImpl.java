package org.example.work.controller.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.work.controller.UserDetailsController;
import org.example.work.dto.UserDetailsDto;
import org.example.work.service.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class UserDetailsControllerImpl implements UserDetailsController {
    private final UserDetailsService userDetailsService;
    private final ObjectMapper objectMapper;

    @Autowired
    public UserDetailsControllerImpl(UserDetailsService userDetailsService, ObjectMapper objectMapper) {
        this.userDetailsService = userDetailsService;
        this.objectMapper = objectMapper;
    }

    @Override
    public void create(String json) {
        try {
            userDetailsService.create(objectMapper.readValue(json, UserDetailsDto.class));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String read(int id) {
        try {
            return objectMapper.writeValueAsString(userDetailsService.read(id));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String update(String json) {
        try {
            return objectMapper.writeValueAsString(userDetailsService.update(objectMapper.readValue(json, UserDetailsDto.class)));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(int id) {
        userDetailsService.delete(id);
    }
}
