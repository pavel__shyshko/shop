package org.example.work.controller.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.work.controller.ProductController;
import org.example.work.dto.ProductDto;
import org.example.work.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class ProductControllerImpl implements ProductController {
    private final ProductService productService;
    private final ObjectMapper objectMapper;

    @Autowired
    public ProductControllerImpl(ProductService productService, ObjectMapper objectMapper) {
        this.productService = productService;
        this.objectMapper = objectMapper;
    }

    @Override
    public void create(String json) {
        try {
            productService.create(objectMapper.readValue(json, ProductDto.class));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String read(int id) {
        try {
            return objectMapper.writeValueAsString(productService.read(id));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String update(String json) {
        try {
            return objectMapper.writeValueAsString(productService.update(objectMapper.readValue(json, ProductDto.class)));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(int id) {
        productService.delete(id);
    }
}
