package org.example.work.controller.impl;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.work.controller.BrandController;
import org.example.work.dto.BrandDto;
import org.example.work.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class BrandControllerImpl implements BrandController {
    private final BrandService brandService;
    private final ObjectMapper objectMapper;

    @Autowired
    public BrandControllerImpl(BrandService brandService, ObjectMapper objectMapper) {
        this.brandService = brandService;
        this.objectMapper = objectMapper;
    }

    @Override
    public void create(String json) {
        try {
            brandService.create(objectMapper.readValue(json, BrandDto.class));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String read(int id) {
        try {
            return objectMapper.writeValueAsString(brandService.read(id));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public String update(String json) {
        try {
            return objectMapper.writeValueAsString(brandService.update(objectMapper.readValue(json, BrandDto.class)));
        } catch (JsonProcessingException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void delete(int id) {
        brandService.delete(id);
    }
}
