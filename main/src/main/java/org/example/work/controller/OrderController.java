package org.example.work.controller;

public interface OrderController {
    void create(String json);

    String read(int id);

    String update(String json);

    void delete(int id);
}
