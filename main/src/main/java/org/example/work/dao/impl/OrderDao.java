package org.example.work.dao.impl;

import org.example.work.dao.OrderDatabase;
import org.example.work.entity.Order;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class OrderDao implements OrderDatabase {
    List<Order> orderList = new ArrayList<>();

    @Override
    public void create(Order order) {
        orderList.add(order);
    }

    @Override
    public Order read(int id) {
        return orderList.get(id);
    }

    @Override
    public Order update(Order order) {
        orderList.set(order.getId() - 1, order);
        return order;
    }

    @Override
    public void delete(int id) {
        orderList.remove(id);
    }
}
