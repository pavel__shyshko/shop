package org.example.work.dao;

import org.example.work.entity.BasketProduct;

public interface BasketProductDatabase {
    void create(BasketProduct basketProduct);

    BasketProduct read(int id);

    BasketProduct update(BasketProduct basketProduct);

    void delete(int id);
}
