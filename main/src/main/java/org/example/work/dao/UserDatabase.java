package org.example.work.dao;

import org.example.work.entity.User;

public interface UserDatabase {
    void create(User user);

    User read(int id);

    User update(User user);

    void delete(int id);
}
