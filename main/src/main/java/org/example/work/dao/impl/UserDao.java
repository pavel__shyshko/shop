package org.example.work.dao.impl;

import org.example.work.dao.UserDatabase;
import org.example.work.entity.User;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDao implements UserDatabase {
    List<User> userList = new ArrayList<User>();

    @Override
    public void create(User user) {
        userList.add(user);
    }

    @Override
    public User read(int id) {
        return userList.get(id);
    }

    @Override
    public User update(User user) {
        userList.set(user.getId() - 1, user);
        return user;
    }

    @Override
    public void delete(int id) {
        userList.remove(id);
    }
}