package org.example.work.dao.impl;

import org.example.work.dao.UserDetailsDatabase;
import org.example.work.entity.UserDetails;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class UserDetailsDao implements UserDetailsDatabase {
    List<UserDetails> detailsList = new ArrayList<>();

    @Override
    public void create(UserDetails details) {
        detailsList.add(details);
    }

    @Override
    public UserDetails read(int id) {
        return detailsList.get(id);
    }

    @Override
    public UserDetails update(UserDetails details) {
        detailsList.set(details.getUserId() - 1, details);
        return details;
    }

    @Override
    public void delete(int id) {
        detailsList.remove(id);
    }
}
