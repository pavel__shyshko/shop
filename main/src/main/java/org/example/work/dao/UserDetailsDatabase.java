package org.example.work.dao;

import org.example.work.entity.UserDetails;

public interface UserDetailsDatabase {
    void create(UserDetails details);

    UserDetails read(int id);

    UserDetails update(UserDetails details);

    void delete(int id);
}
