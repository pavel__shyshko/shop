package org.example.work.dao;

import org.example.work.entity.Category;

public interface CategoryDatabase {
    void create(Category category);

    Category read(int id);

    Category update(Category category);

    void delete(int id);
}
