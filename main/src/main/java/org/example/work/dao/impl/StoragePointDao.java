package org.example.work.dao.impl;

import org.example.work.dao.StoragePointDatabase;
import org.example.work.entity.StoragePoint;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class StoragePointDao implements StoragePointDatabase {
    List<StoragePoint> storagePointList = new ArrayList<>();

    @Override
    public void create(StoragePoint point) {
        storagePointList.add(point);

    }

    @Override
    public StoragePoint read(int id) {
        return storagePointList.get(id);
    }

    @Override
    public StoragePoint update(StoragePoint point) {
        storagePointList.set(point.getId() - 1, point);
        return point;
    }

    @Override
    public void delete(int id) {
        storagePointList.remove(id);
    }
}
