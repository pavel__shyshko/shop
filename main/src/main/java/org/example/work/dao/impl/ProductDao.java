package org.example.work.dao.impl;

import org.example.work.connection.ConnectionHolder;
import org.example.work.dao.ProductDatabase;
import org.example.work.entity.Product;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

@Repository
public class ProductDao implements ProductDatabase {
    private final ConnectionHolder connectionHolder;

    @Autowired
    public ProductDao(ConnectionHolder connectionHolder) {
        this.connectionHolder = connectionHolder;
    }

    @Override
    public void create(Product product) {
        String sql = "insert into product (id, name, brandId, categoryId, description, price, color, size, creationDate, discount) values(?,?,?,?,?,?,?,?,?,?)";
        try (PreparedStatement preparedStatement = connectionHolder.getConnection().prepareStatement(sql)) {
            preparedStatement.setInt(1, product.getId());
            preparedStatement.setString(2, product.getName());
            preparedStatement.setInt(3, product.getBrandId());
            preparedStatement.setInt(4, product.getCategoryId());
            preparedStatement.setString(5, product.getDescription());
            preparedStatement.setDouble(6, product.getPrice());
            preparedStatement.setString(7, product.getColor());
            preparedStatement.setString(8, product.getSize());
            preparedStatement.setDate(9, product.getCreationDate());
            preparedStatement.setDouble(10, product.getDiscount());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public Product read(int id) {
        Product product = null;
        String sql = "select * from product where id = ?";
        try (PreparedStatement preparedStatement = connectionHolder.getConnection().prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                if (Integer.valueOf(id).equals(resultSet.getInt("id"))) {
                    product = new Product();
                    product.setId(resultSet.getInt("id"));
                    product.setName(resultSet.getString("name"));
                    product.setBrandId(resultSet.getInt("brandId"));
                    product.setCategoryId(resultSet.getInt("categoryId"));
                    product.setDescription(resultSet.getString("description"));
                    product.setPrice(resultSet.getDouble("price"));
                    product.setColor(resultSet.getString("color"));
                    product.setSize(resultSet.getString("size"));
                    product.setCreationDate(resultSet.getDate("creationDate"));
                    product.setDiscount(resultSet.getDouble("discount"));
                }
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return product;
    }

    @Override
    public Product update(Product product) {

        String sql = "Update product set name=?, brandId=?, categoryId=?, description=?, price=?, color=?, size=?, discount=? where id=?";
        try (PreparedStatement preparedStatement = connectionHolder.getConnection().prepareStatement(sql)) {
            preparedStatement.setString(1, product.getName());
            preparedStatement.setInt(2, product.getBrandId());
            preparedStatement.setInt(3, product.getCategoryId());
            preparedStatement.setString(4, product.getDescription());
            preparedStatement.setDouble(5, product.getPrice());
            preparedStatement.setString(6, product.getColor());
            preparedStatement.setString(7, product.getSize());
            preparedStatement.setDate(8, product.getCreationDate());
            preparedStatement.setDouble(9, product.getDiscount());
            preparedStatement.setInt(10, product.getId());
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return product;
    }

    @Override
    public void delete(int id) {
        String sql = "delete from product where id = ?";
        try (PreparedStatement preparedStatement = connectionHolder.getConnection().prepareStatement(sql)) {
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
