package org.example.work.dao.impl;

import org.example.work.dao.BasketProductDatabase;
import org.example.work.entity.BasketProduct;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BasketProductDao implements BasketProductDatabase {
    List<BasketProduct> basketProductList = new ArrayList<>();

    @Override
    public void create(BasketProduct basketProduct) {
        basketProductList.add(basketProduct);
    }

    @Override
    public BasketProduct read(int id) {
        return basketProductList.get(id);
    }

    @Override
    public BasketProduct update(BasketProduct basketProduct) {
        basketProductList.set(basketProduct.getProductId() - 1, basketProduct);
        return basketProduct;
    }

    @Override
    public void delete(int id) {
        basketProductList.remove(id);
    }
}
