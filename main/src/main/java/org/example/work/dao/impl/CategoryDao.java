package org.example.work.dao.impl;

import org.example.work.dao.CategoryDatabase;
import org.example.work.entity.Category;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class CategoryDao implements CategoryDatabase {
    List<Category> categoryList = new ArrayList<Category>();

    @Override
    public void create(Category category) {
        categoryList.add(category);

    }

    @Override
    public Category read(int id) {
        return categoryList.get(id);
    }

    @Override
    public Category update(Category category) {
        categoryList.set(category.getId() - 1, category);
        return category;
    }

    @Override
    public void delete(int id) {
        categoryList.remove(id);
    }
}
