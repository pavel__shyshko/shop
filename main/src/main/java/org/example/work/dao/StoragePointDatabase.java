package org.example.work.dao;

import org.example.work.entity.StoragePoint;

public interface StoragePointDatabase {
    void create(StoragePoint point);

    StoragePoint read(int id);

    StoragePoint update(StoragePoint point);

    void delete(int id);
}
