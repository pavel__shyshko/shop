package org.example.work.dao;

import org.example.work.entity.Order;

public interface OrderDatabase {
    void create(Order order);

    Order read(int id);

    Order update(Order order);

    void delete(int id);
}
