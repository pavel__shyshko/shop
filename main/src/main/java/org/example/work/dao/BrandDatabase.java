package org.example.work.dao;

import org.example.work.entity.Brand;

public interface BrandDatabase {
    void create(Brand brand);

    Brand read(int id);

    Brand update(Brand brand);

    void delete(int id);
}
