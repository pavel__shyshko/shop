package org.example.work.dao;

import org.example.work.entity.Product;

public interface ProductDatabase {
    void create(Product product);

    Product read(int id);

    Product update(Product product);

    void delete(int id);
}
