package org.example.work.dao.impl;

import org.example.work.dao.BrandDatabase;
import org.example.work.entity.Brand;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class BrandDao implements BrandDatabase {
    List<Brand> brandList = new ArrayList<Brand>();

    @Override
    public void create(Brand brand) {
        brandList.add(brand);
    }

    @Override
    public Brand read(int id) {
        return brandList.get(id);
    }

    @Override
    public Brand update(Brand brand) {
        brandList.set(brand.getId() - 1, brand);
        return brand;
    }

    @Override
    public void delete(int id) {
        brandList.remove(id);
    }
}
