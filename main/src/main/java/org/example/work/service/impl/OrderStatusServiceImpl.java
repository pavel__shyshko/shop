package org.example.work.service.impl;

import org.example.work.entity.OrderStatus;
import org.example.work.service.OrderStatusService;
import org.springframework.stereotype.Service;

@Service
public class OrderStatusServiceImpl implements OrderStatusService {
    @Override
    public void create(OrderStatus orderStatus) {
    }

    @Override
    public OrderStatus read(int id) {
        return null;
    }

    @Override
    public OrderStatus update(OrderStatus orderStatus) {
        return null;
    }

    @Override
    public void delete(int id) {
    }
}
