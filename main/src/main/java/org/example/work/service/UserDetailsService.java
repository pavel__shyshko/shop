package org.example.work.service;

import org.example.work.dto.UserDetailsDto;

public interface UserDetailsService {
    void create(UserDetailsDto userDetailsDto);

    UserDetailsDto read(int id);

    UserDetailsDto update(UserDetailsDto userDetailsDto);

    void delete(int id);
}
