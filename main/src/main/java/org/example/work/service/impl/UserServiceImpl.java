package org.example.work.service.impl;

import org.example.work.dao.UserDatabase;
import org.example.work.dto.UserDto;
import org.example.work.entity.User;
import org.example.work.mapper.UserMapper;
import org.example.work.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserServiceImpl implements UserService {
    private final UserDatabase userDatabase;
    private final UserMapper userMapper;

    @Autowired
    public UserServiceImpl(UserDatabase userDatabase, UserMapper userMapper) {
        this.userDatabase = userDatabase;
        this.userMapper = userMapper;
    }

    @Override
    public void create(UserDto userDto) {
        userDatabase.create(userMapper.userDtoToUser(userDto));
    }

    @Override
    public UserDto read(int id) {
        return userMapper.userToUserDto(userDatabase.read(id));
    }

    @Override
    public UserDto update(UserDto userDto) {
        User user = userDatabase.update(userMapper.userDtoToUser(userDto));
        return userMapper.userToUserDto(user);
    }

    @Override
    public void delete(int id) {
        userDatabase.delete(id);
    }
}
