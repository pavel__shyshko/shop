package org.example.work.service.impl;

import org.example.work.dao.ProductDatabase;
import org.example.work.dto.ProductDto;
import org.example.work.entity.Product;
import org.example.work.mapper.ProductMapper;
import org.example.work.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ProductServiceImpl implements ProductService {
    private final ProductDatabase productDatabase;
    private final ProductMapper productMapper;

    @Autowired
    public ProductServiceImpl(ProductDatabase productDatabase, ProductMapper productMapper) {
        this.productDatabase = productDatabase;
        this.productMapper = productMapper;
    }

    @Override
    public void create(ProductDto productDto) {
        productDatabase.create(productMapper.productDtoToProduct(productDto));
    }

    @Override
    public ProductDto read(int id) {
        return productMapper.productToProductDto(productDatabase.read(id));
    }

    @Override
    public ProductDto update(ProductDto productDto) {
        Product product = productDatabase.update(productMapper.productDtoToProduct(productDto));
        return productMapper.productToProductDto(product);
    }

    @Override
    public void delete(int id) {
        productDatabase.delete(id);
    }
}
