package org.example.work.service;

import org.example.work.dto.BrandDto;

public interface BrandService {
    void create(BrandDto brandDto);

    BrandDto read(int id);

    BrandDto update(BrandDto brandDto);

    void delete(int id);
}
