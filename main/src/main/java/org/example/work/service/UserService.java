package org.example.work.service;

import org.example.work.dto.UserDto;

public interface UserService {
    void create(UserDto userDto);

    UserDto read(int id);

    UserDto update(UserDto userDto);

    void delete(int id);
}
