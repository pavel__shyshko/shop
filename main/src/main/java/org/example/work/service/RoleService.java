package org.example.work.service;

import org.example.work.entity.Role;

public interface RoleService {
    void create();

    Role read();

    void update();

    void delete();
}
