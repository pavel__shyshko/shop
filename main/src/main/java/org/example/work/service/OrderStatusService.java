package org.example.work.service;

import org.example.work.entity.OrderStatus;

public interface OrderStatusService {
    void create(OrderStatus orderStatus);

    OrderStatus read(int id);

    OrderStatus update(OrderStatus orderStatus);

    void delete(int id);
}
