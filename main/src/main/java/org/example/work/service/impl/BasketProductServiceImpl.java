package org.example.work.service.impl;

import org.example.work.dao.BasketProductDatabase;
import org.example.work.dto.BasketProductDto;
import org.example.work.entity.BasketProduct;
import org.example.work.mapper.BasketProductMapper;
import org.example.work.service.BasketProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BasketProductServiceImpl implements BasketProductService {
    private final BasketProductDatabase basketProductDatabase;
    private final BasketProductMapper basketProductMapper;

    @Autowired
    public BasketProductServiceImpl(BasketProductDatabase basketProductDatabase, BasketProductMapper basketProductMapper) {
        this.basketProductDatabase = basketProductDatabase;
        this.basketProductMapper = basketProductMapper;
    }

    @Override
    public void create(BasketProductDto basketProductDto) {
        basketProductDatabase.create(basketProductMapper.basketProductDtoToBasketProduct(basketProductDto));
    }

    @Override
    public BasketProductDto read(int id) {
        return basketProductMapper.basketProductToBasketProductDto(basketProductDatabase.read(id));
    }

    @Override
    public BasketProductDto update(BasketProductDto basketProductDto) {
        BasketProduct basketProduct = basketProductDatabase.update(basketProductMapper.basketProductDtoToBasketProduct(basketProductDto));
        return basketProductMapper.basketProductToBasketProductDto(basketProduct);
    }

    @Override
    public void delete(int id) {
        basketProductDatabase.delete(id);
    }
}
