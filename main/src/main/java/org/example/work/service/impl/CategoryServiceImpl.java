package org.example.work.service.impl;

import org.example.work.dao.CategoryDatabase;
import org.example.work.dto.CategoryDto;
import org.example.work.entity.Category;
import org.example.work.mapper.CategoryMapper;
import org.example.work.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CategoryServiceImpl implements CategoryService {
    private final CategoryDatabase categoryDatabase;
    private final CategoryMapper categoryMapper;

    @Autowired
    public CategoryServiceImpl(CategoryDatabase categoryDatabase, CategoryMapper categoryMapper) {
        this.categoryDatabase = categoryDatabase;
        this.categoryMapper = categoryMapper;
    }

    @Override
    public void create(CategoryDto categoryDto) {
        categoryDatabase.create(categoryMapper.categoryDtoToCategory(categoryDto));
    }

    @Override
    public CategoryDto read(int id) {
        return categoryMapper.categoryToCategoryDto(categoryDatabase.read(id));
    }

    @Override
    public CategoryDto update(CategoryDto categoryDto) {
        Category category = categoryDatabase.update(categoryMapper.categoryDtoToCategory(categoryDto));
        return categoryMapper.categoryToCategoryDto(category);
    }

    @Override
    public void delete(int id) {
        categoryDatabase.delete(id);
    }
}
