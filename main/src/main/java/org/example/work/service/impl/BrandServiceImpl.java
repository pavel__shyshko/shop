package org.example.work.service.impl;

import org.example.work.dao.BrandDatabase;
import org.example.work.dto.BrandDto;
import org.example.work.entity.Brand;
import org.example.work.mapper.BrandMapper;
import org.example.work.service.BrandService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class BrandServiceImpl implements BrandService {
    private final BrandDatabase brandDatabase;
    private final BrandMapper brandMapper;

    @Autowired
    public BrandServiceImpl(BrandDatabase brandDatabase, BrandMapper brandMapper) {
        this.brandDatabase = brandDatabase;
        this.brandMapper = brandMapper;
    }

    @Override
    public void create(BrandDto brandDto) {
        brandDatabase.create(brandMapper.brandDtoToBrand(brandDto));
    }

    @Override
    public BrandDto read(int id) {
        return brandMapper.brandToBrandDto(brandDatabase.read(id));
    }

    @Override
    public BrandDto update(BrandDto brandDto) {
        Brand brand = brandDatabase.update(brandMapper.brandDtoToBrand(brandDto));
        return brandMapper.brandToBrandDto(brand);
    }

    @Override
    public void delete(int id) {
        brandDatabase.delete(id);
    }
}
