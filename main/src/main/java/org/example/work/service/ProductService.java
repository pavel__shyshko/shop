package org.example.work.service;

import org.example.work.dto.ProductDto;

public interface ProductService {
    void create(ProductDto productDto);

    ProductDto read(int id);

    ProductDto update(ProductDto productDto);

    void delete(int id);
}
