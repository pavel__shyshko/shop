package org.example.work.service.impl;

import org.example.work.dao.OrderDatabase;
import org.example.work.dto.OrderDto;
import org.example.work.entity.Order;
import org.example.work.mapper.OrderMapper;
import org.example.work.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderServiceImpl implements OrderService {
    private final OrderDatabase orderDatabase;
    private final OrderMapper orderMapper;

    @Autowired
    public OrderServiceImpl(OrderDatabase orderDatabase, OrderMapper orderMapper) {
        this.orderDatabase = orderDatabase;
        this.orderMapper = orderMapper;
    }

    @Override
    public void create(OrderDto orderDto) {
        orderDatabase.create(orderMapper.orderDtoToOrder(orderDto));
    }

    @Override
    public OrderDto read(int id) {
        return orderMapper.orderToOrderDto(orderDatabase.read(id));
    }

    @Override
    public OrderDto update(OrderDto orderDto) {
        Order order = orderDatabase.update(orderMapper.orderDtoToOrder(orderDto));
        return orderMapper.orderToOrderDto(order);
    }

    @Override
    public void delete(int id) {
        orderDatabase.delete(id);
    }
}
