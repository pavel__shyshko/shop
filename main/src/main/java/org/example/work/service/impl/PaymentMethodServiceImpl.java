package org.example.work.service.impl;

import org.example.work.entity.PaymentMethod;
import org.example.work.service.PaymentMethodService;
import org.springframework.stereotype.Service;

@Service
public class PaymentMethodServiceImpl implements PaymentMethodService {
    @Override
    public void create(PaymentMethod paymentMethod) {
    }

    @Override
    public PaymentMethod read(int id) {
        return null;
    }

    @Override
    public PaymentMethod update(PaymentMethod paymentMethod) {
        return null;
    }

    @Override
    public void delete(int id) {
    }
}
