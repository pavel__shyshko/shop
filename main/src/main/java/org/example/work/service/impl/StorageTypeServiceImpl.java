package org.example.work.service.impl;

import org.example.work.entity.StorageType;
import org.example.work.service.StorageTypeService;
import org.springframework.stereotype.Service;

@Service
public class StorageTypeServiceImpl implements StorageTypeService {
    @Override
    public void create() {
    }

    @Override
    public StorageType read() {
        return null;
    }

    @Override
    public void update() {
    }

    @Override
    public void delete() {
    }
}
