package org.example.work.service.impl;

import org.example.work.dao.StoragePointDatabase;
import org.example.work.dto.StoragePointDto;
import org.example.work.entity.StoragePoint;
import org.example.work.mapper.StoragePointMapper;
import org.example.work.service.StoragePointService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class StoragePointServiceImpl implements StoragePointService {
    private final StoragePointDatabase storagePointDatabase;
    private final StoragePointMapper storagePointMapper;

    @Autowired
    public StoragePointServiceImpl(StoragePointDatabase storagePointDatabase, StoragePointMapper storagePointMapper) {
        this.storagePointDatabase = storagePointDatabase;
        this.storagePointMapper = storagePointMapper;
    }

    @Override
    public void create(StoragePointDto storagePointDto) {
        storagePointDatabase.create(storagePointMapper.storagePointDtoToStoragePoint(storagePointDto));
    }

    @Override
    public StoragePointDto read(int id) {
        return storagePointMapper.storagePointToStoragePointDto(storagePointDatabase.read(id));
    }

    @Override
    public StoragePointDto update(StoragePointDto storagePointDto) {
        StoragePoint storagePoint = storagePointDatabase.update(storagePointMapper.storagePointDtoToStoragePoint(storagePointDto));
        return storagePointMapper.storagePointToStoragePointDto(storagePoint);
    }

    @Override
    public void delete(int id) {
        storagePointDatabase.delete(id);
    }
}
