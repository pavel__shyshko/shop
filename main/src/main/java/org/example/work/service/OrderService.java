package org.example.work.service;

import org.example.work.dto.OrderDto;

public interface OrderService {
    void create(OrderDto orderDto);

    OrderDto read(int id);

    OrderDto update(OrderDto orderDto);

    void delete(int id);
}
