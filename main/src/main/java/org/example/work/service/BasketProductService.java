package org.example.work.service;

import org.example.work.dto.BasketProductDto;

public interface BasketProductService {
    void create(BasketProductDto basketProductDto);

    BasketProductDto read(int id);

    BasketProductDto update(BasketProductDto basketProductDto);

    void delete(int id);
}
