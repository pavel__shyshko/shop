package org.example.work.service;

import org.example.work.dto.StoragePointDto;

public interface StoragePointService {
    void create(StoragePointDto storagePointDto);

    StoragePointDto read(int id);

    StoragePointDto update(StoragePointDto storagePointDto);

    void delete(int id);
}
