package org.example.work.service.impl;

import org.example.work.dao.UserDetailsDatabase;
import org.example.work.dto.UserDetailsDto;
import org.example.work.entity.UserDetails;
import org.example.work.mapper.UserDetailsMapper;
import org.example.work.service.UserDetailsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
    private final UserDetailsDatabase userDetailsDatabase;
    private final UserDetailsMapper userDetailsMapper;

    @Autowired
    public UserDetailsServiceImpl(UserDetailsDatabase userDetailsDatabase, UserDetailsMapper userDetailsMapper) {
        this.userDetailsDatabase = userDetailsDatabase;
        this.userDetailsMapper = userDetailsMapper;
    }

    @Override
    public void create(UserDetailsDto userDetailsDto) {
        userDetailsDatabase.create(userDetailsMapper.userDetailsDtoToUserDetails(userDetailsDto));

    }

    @Override
    public UserDetailsDto read(int id) {
        return userDetailsMapper.detailsToUserDetailsDto(userDetailsDatabase.read(id));
    }

    @Override
    public UserDetailsDto update(UserDetailsDto userDetailsDto) {
        UserDetails userDetails = userDetailsDatabase.update(userDetailsMapper.userDetailsDtoToUserDetails(userDetailsDto));
        return userDetailsMapper.detailsToUserDetailsDto(userDetails);
    }

    @Override
    public void delete(int id) {
        userDetailsDatabase.delete(id);
    }
}
