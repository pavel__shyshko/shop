package org.example.work.service;

import org.example.work.dto.CategoryDto;

public interface CategoryService {
    void create(CategoryDto categoryDto);

    CategoryDto read(int id);

    CategoryDto update(CategoryDto categoryDto);

    void delete(int id);
}
