package org.example.work.service;

import org.example.work.entity.PaymentMethod;

public interface PaymentMethodService {
    void create(PaymentMethod paymentMethod);

    PaymentMethod read(int id);

    PaymentMethod update(PaymentMethod paymentMethod);

    void delete(int id);
}
