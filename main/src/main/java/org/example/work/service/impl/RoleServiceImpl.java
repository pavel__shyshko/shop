package org.example.work.service.impl;

import org.example.work.entity.Role;
import org.example.work.service.RoleService;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {
    @Override
    public void create() {
    }

    @Override
    public Role read() {
        return null;
    }

    @Override
    public void update() {
    }

    @Override
    public void delete() {
    }
}
