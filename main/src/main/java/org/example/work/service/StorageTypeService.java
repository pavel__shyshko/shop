package org.example.work.service;

import org.example.work.entity.StorageType;

public interface StorageTypeService {
    void create();

    StorageType read();

    void update();

    void delete();
}
