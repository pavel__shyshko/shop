package org.example.work.configuration;

import org.postgresql.ds.PGSimpleDataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

import javax.sql.DataSource;

@PropertySource("classpath:application.properties")
@Component
@Configuration
public class DatabaseConfiguration {

    @Value("${url}")
    private String dbUrl;
    @Value("${username}")
    private String dbUser;
    @Value("${password}")
    private String dbPassword;
    @Value("${driver}")
    private String driver;

    @Bean
    public DataSource dataSource() {
        PGSimpleDataSource dataSource = new PGSimpleDataSource();
        dataSource.setServerNames(new String[]{"localhost"});
        dataSource.setPortNumbers(new int[]{5438});
        dataSource.setDatabaseName(dbUrl);
        dataSource.setUser(dbUser);
        dataSource.setPassword(dbPassword);
        return dataSource;
    }
}
