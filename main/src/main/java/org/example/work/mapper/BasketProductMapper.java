package org.example.work.mapper;

import org.example.work.dto.BasketProductDto;
import org.example.work.entity.BasketProduct;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface BasketProductMapper {
    BasketProductDto basketProductToBasketProductDto(BasketProduct basketProduct);

    BasketProduct basketProductDtoToBasketProduct(BasketProductDto basketProductDto);
}
