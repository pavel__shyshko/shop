package org.example.work.mapper;

import org.example.work.dto.UserDto;
import org.example.work.entity.User;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserMapper {
    UserDto userToUserDto(User user);

    User userDtoToUser(UserDto userDto);
}

