package org.example.work.mapper;

import org.example.work.dto.StoragePointDto;
import org.example.work.entity.StoragePoint;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface StoragePointMapper {
    StoragePointDto storagePointToStoragePointDto(StoragePoint storagePoint);

    StoragePoint storagePointDtoToStoragePoint(StoragePointDto storagePointDto);
}
