package org.example.work.mapper;

import org.example.work.dto.UserDetailsDto;
import org.example.work.entity.UserDetails;
import org.mapstruct.Mapper;

@Mapper(componentModel = "spring")
public interface UserDetailsMapper {
    UserDetailsDto detailsToUserDetailsDto(UserDetails userDetails);

    UserDetails userDetailsDtoToUserDetails(UserDetailsDto userDetailsDto);
}
